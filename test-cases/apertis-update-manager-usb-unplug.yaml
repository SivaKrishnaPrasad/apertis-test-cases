metadata:
  name: apertis-update-manager-usb-unplug
  format: "Apertis Test Definition 1.0"
  image-types:
    fixedfunction: [ armhf, arm64 ]
  image-deployment:
    - OSTree
  type: functional
  exec-type: manual
  priority: critical
  maintainer: "Apertis Project"
  description: "Test the apertis-update-manager automatic update interrupted by mass storage removing."

  resources:
    - "A static encrypted update bundle file of the same architecture, variant and version as the testing image"
    - "A Fat32 USB flash drive, preloaded with the encrypted update bundle at the root of the disk"
    - "The latest static encrypted update file can be downloaded at the same location than the Apertis image. It has the same basename, and a '.delta.enc' extension"
    - "The static encrypted update file should be copied to the flash drive using the name 'static-update.bundle.enc'."
    - "A PC must be connected to DUT serial port"

  expected:
    - "The update wasn't applied"
    - "System boots using the initial deployment"

run:
  steps:
    - "Check the initial deployment"
    - $ sudo ostree admin status
    - "Prepare the copy of commit and deploy to allow the upgrade to the same version"
    - "Command below shows you an initial commit ID, for instance"
    - |
        $ export BOOTID=$(sudo ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p'); echo $BOOTID
    - "Get the Collection ID and ref"
    -  $ export CID=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 1 -d ' '); echo COLLECTION_ID=$CID
    -  $ export REF=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 2 -d ' '); echo REF=$REF
    - "Create the list of files to skip and enshure there are some files in these directories"
    - $ ls -1d /usr/share/locale /usr/share/man /usr/share/zoneinfo > /tmp/skip
    - $ du -sh /usr/share/locale /usr/share/man /usr/share/zoneinfo
    - "Create the commit with changed timestamp and skipped list from above to allow upgrade with recent update file"
    - |
        $ export NEWID=$(sudo ostree commit --orphan --tree=ref=$BOOTID --add-metadata-string=ostree.collection-binding=$CID --bind-ref=$REF --timestamp="1 year ago" --skip-list=/tmp/skip); echo "New commit: $NEWID"
    - "Deploy the prepared commit"
    - $ sudo ostree admin upgrade --allow-downgrade --deploy-only --override-commit=$NEWID --reboot
    - "Wait until the system is booted again and check the deployment"
    - $ sudo ostree admin status
    - "The booted commit (started with '*') must have ID which we prepare and the initial commit ID should be marked as '(rollback)'"
    - "Check booted deployment have no file objects which we skip"
    - $ du -sh /usr/share/locale /usr/share/man /usr/share/zoneinfo
    - "Remove the initial deployment"
    - $ sudo ostree admin undeploy 1
    - "Add udev rule for limiting the maximal read speed from USB drive to 1MBps."
    - $ sudo tee /etc/udev/rules.d/99-usblimit.rules <<"EOF"
    - |
        >KERNEL=="sd*", SUBSYSTEMS=="usb", ACTION=="add", ENV{DEVTYPE}=="disk", ENV{MAJOR}=="8", RUN+="/bin/sh -c 'echo $env{MAJOR}:$env{MINOR} 1048576 > /sys/fs/cgroup/blkio/blkio.throttle.read_bps_device'"
        EOF
    - "Reboot the system"
    - "Check the current deployment"
    - $ sudo ostree admin status
    - "Need to monitor the journal log to catch events in time"
    - $ sudo journalctl -ef --unit apertis-update-manager &
    - "Plug the USB flash drive into the device"
    - "The update starts automatically"
    - "Wait while the upgrade starts (5-20 seconds depending on your HW) and remove the USB flash drive before it is completed:"
    - |
        >Dec 19 22:31:46 apertis apertis-update-[390]: mount added : /media/test
        Dec 19 22:31:46 apertis apertis-update-[390]: mount added : /media/update
        Dec 19 22:31:46 apertis apertis-update-[390]: Ostree static delta starting
        Dec 19 22:31:46 apertis apertis-update-[390]: Cannot check the ID in black list: No such file or directory
    - "Update should be interrupted with error messages similar to"
    - |
        >[   59.542717] scsi 1:0:0:0: rejecting I/O to offline device
        [   59.548183] scsi 1:0:0:0: rejecting I/O to offline device
        [   59.553633] scsi 1:0:0:0: rejecting I/O to dead device
        [   59.558840] scsi 1:0:0:0: rejecting I/O to dead device
        [   59.564239] print_req_error: I/O error, dev sda, sector 24393
        [   59.570120] print_req_error: I/O error, dev sda, sector 24633
        Dec 19 22:31:49 apertis systemd[1]: apertis-update-manager.service: Main process exited, code=killed, status=7/BUS
        Dec 19 22:31:49 apertis systemd[1]: apertis-update-manager.service: Unit entered failed state.
        Dec 19 22:31:49 apertis systemd[1]: apertis-update-manager.service: Failed with result 'signal'.
        Dec 19 22:31:49 apertis systemd[1]: apertis-update-manager.service: Service hold-off time over, scheduling restart.
        Dec 19 22:31:49 apertis systemd[1]: Stopped Apertis update manager.
        Dec 19 22:31:49 apertis systemd[1]: Starting Apertis update manager...
    - "Check the current deployment is the same that at the beginning of the test"
    - $ sudo ostree admin status
    - "Repeat the test several times by plug and unplug the USB stick"
    - "Reboot the system"
    - $ sudo reboot
    - "Check the current deployment is the same that at the beginning of the test"
    - $ sudo ostree admin status
 
